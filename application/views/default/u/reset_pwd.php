<?php require_once VIEWPATH . "$theme_id/inc/header.inc.php";?>
<div id="main" class="main layui-clear">
    <div class="wrap">
        <div class="content">
            <?php require_once VIEWPATH . "$theme_id/u/inc/nav.inc.php";?>
            <div class="user-mine">
                <?php if (!empty($user['email'])): ?>
                    <div class="layui-form layui-form-pane">
                        <form method="post" onsubmit="return account_reset_pwd();">
                            <div class="layui-form-item">
                                <label for="email" class="layui-form-label">邮箱</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="email" name="email" class="layui-input" value="<?=$user['email']?>" disabled>
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <label for="pwd" class="layui-form-label">原密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="pwd" name="pwd" class="layui-input">
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <label for="new_pwd" class="layui-form-label">新密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="new_pwd" name="new_pwd" class="layui-input">
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <button class="layui-btn" type="submit">确认</button>
                            </div>
                        </form>
                    </div>
                <?php else: ?>
                    <p>请先 <a class="layui-btn layui-btn-mini" href="javascript:;" onclick="bind_email_show();">绑定邮箱</a></p>
                <?php endif;?>
            </div>
            <div id="LAY-page"></div>
        </div>
    </div>
    <?php require_once VIEWPATH . "$theme_id/u/inc/sidebar.inc.php";?>
</div>
<script type="text/javascript">
$(function(){
    $('#pwd').focus();
});

//修改密码
function account_reset_pwd(){
    var $pwd = $('#pwd');
    var $new_pwd = $('#new_pwd');
    var pwd = $pwd.val();
    var new_pwd = $new_pwd.val();

    if(!simple_validate.required(pwd)){
        layer.msg('请填写原密码');
        $pwd.focus();
        return false;
    }

    if(!simple_validate.range(pwd, 6, 16)){
        layer.msg('原密码长度为6-16位');
        $pwd.focus();
        return false;
    }

    if(!simple_validate.required(new_pwd)){
        layer.msg('请填写新密码');
        $new_pwd.focus();
        return false;
    }

    if(!simple_validate.range(new_pwd, 6, 16)){
        layer.msg('新密码长度为6-16位');
        $new_pwd.focus();
        return false;
    }

    var post_data = {
        pwd: pwd,
        new_pwd: new_pwd
    };
    //Geetest验证码
    var $captcha = $('#captcha');
    if($captcha.length > 0){
        post_data.geetest_challenge = $(':input[name=geetest_challenge]').val();
        post_data.geetest_validate = $(':input[name=geetest_validate]').val();
        post_data.geetest_seccode = $(':input[name=geetest_seccode]').val();
    }

    layer.load();
    $.post(
        '/api/account/reset_pwd',
        post_data,
        function(json){
            layer.closeAll('loading');
            if(json.error_code == 'ok'){
                layer.msg('密码修改成功');
                $pwd.val('').focus();
                $new_pwd.val('');
            }
            else{
                show_error(json.error_code);
            }
        },
        'json'
    );

    return false;
}
</script>
<?php require_once VIEWPATH . "$theme_id/inc/footer.inc.php";?>